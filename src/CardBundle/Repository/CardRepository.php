<?php

namespace App\CardBundle\Repository;

use App\CardBundle\Entity\Card;
use Doctrine\ORM\EntityRepository;
use PDO;

class CardRepository extends EntityRepository
{
    /**
     * @param null|string $serie
     * @return int
     * @throws \Doctrine\DBAL\DBALException
     */
    public function generateNumber(?string $serie)
    {
        $conn = $this->getEntityManager()->getConnection();

        $sth = $conn->prepare("
            SELECT cards_main.card_number+1 AS available
            FROM cards AS cards_main
            LEFT JOIN cards AS cards_joined ON cards_main.card_number+1 = cards_joined.card_number AND cards_joined.card_serie = cards_main.card_serie
            WHERE cards_main.card_serie = :serie AND cards_joined.card_number IS NULL
            ORDER BY cards_main.card_number 
            LIMIT 1;
        ");
        $sth->execute([
            ":serie" => $serie,
        ]);
        $result = $sth->fetch(PDO::FETCH_ASSOC);
        if (!$result) {
            $number = 1;
        } else {
            $number = $result["available"];
        }

        return $number;
    }

    /**
     * @return int
     * @throws \Doctrine\DBAL\DBALException
     */
    public function checkExpiredCards()
    {
        $conn = $this->getEntityManager()->getConnection();

        $sth = $conn->prepare("
            UPDATE cards
            SET status = :status_expired 
            WHERE status != :status_expired AND expires_at <= :expiry_date
        ");
        $sth->execute([
            ":status_expired" => Card::STATUS_EXPIRED,
            ":expiry_date"    => (new \DateTime())->format("Y-m-d H:i:s"),
        ]);
        $affected_row_count = $sth->rowCount();

        return $affected_row_count;
    }

}

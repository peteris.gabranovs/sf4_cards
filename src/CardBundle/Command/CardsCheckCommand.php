<?php

namespace App\CardBundle\Command;

use App\CardBundle\Entity\Card;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class CardsCheckCommand
 * @package App\CardBundle\Command
 */
class CardsCheckCommand extends ContainerAwareCommand
{
    protected function configure()
    {
//        php bin/console cards:check
        $this->setName('cards:check');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Checking expired cards...');
        $cardRepository = $this->getContainer()->get("doctrine")->getManager()->getRepository(Card::class);
        $result = $cardRepository->checkExpiredCards();
        $output->writeln('Found ' . $result . ' expired cards');
    }
}

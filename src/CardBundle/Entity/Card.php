<?php

namespace App\CardBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index;

/**
 *
 * php bin/console make:entity --regenerate App\CardBundle\Entity
 * php bin/console doctrine:schema:update --force
 *
 * @ORM\Table(name="cards", indexes={
 *     @Index(name="cards_card_number", columns={"card_number"}),
 *     @Index(name="cards_card_serie", columns={"card_serie"}),
 *     @Index(name="cards_issued_at", columns={"issued_at"}),
 *     @Index(name="cards_expires_at", columns={"expires_at"}),
 *     @Index(name="cards_used_at", columns={"used_at"}),
 *     @Index(name="cards_balance", columns={"balance"})
 * })
 * @ORM\Entity(repositoryClass="App\CardBundle\Repository\CardRepository")
 */
class Card
{

    const STATUS_ACTIVE   = 1;
    const STATUS_INACTIVE = 0;
    const STATUS_EXPIRED  = -1;

    /**
     * @return array
     */
    public static function getStatusList()
    {
        return [
            'Active' => self::STATUS_ACTIVE,
            'Inactive' => self::STATUS_INACTIVE,
            'Expired' => self::STATUS_EXPIRED,
        ];
    }

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="bigint", length=16,  nullable=true)
     */
    private $card_number;

    /**
     * @ORM\Column(type="string", length=4, nullable=true)
     */
    private $card_serie;

    /**
     * @ORM\Column(type="datetime")
     */
    private $issued_at;

    /**
     * @ORM\Column(type="datetime")
     */
    private $expires_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $used_at;

    /**
     * @ORM\Column(type="decimal", precision=20, scale=2, options={"default":"0"})
     */
    private $balance = 0;

    /**
     * @ORM\Column(type="integer", length=1, nullable=true)
     */
    private $status = self::STATUS_INACTIVE;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id)
    {
        $this->id = $id;
        return $this;
    }

    public function getCardNumber(): ?string
    {
        return $this->card_number;
    }

    public function setCardNumber(?string $card_number): self
    {
        $this->card_number = $card_number;

        return $this;
    }

    public function getCardSerie(): ?string
    {
        return $this->card_serie;
    }

    public function setCardSerie(?string $card_serie): self
    {
        $this->card_serie = $card_serie;

        return $this;
    }

    public function getBalance()
    {
        return $this->balance;
    }

    public function setBalance($balance): self
    {
        $this->balance = $balance;

        return $this;
    }

    public function getIssuedAt(): ?\DateTimeInterface
    {
        return $this->issued_at;
    }

    public function setIssuedAt(\DateTimeInterface $issued_at): self
    {
        $this->issued_at = $issued_at;

        return $this;
    }

    public function getExpiresAt(): ?\DateTimeInterface
    {
        return $this->expires_at;
    }

    public function setExpiresAt(\DateTimeInterface $expires_at): self
    {
        $this->expires_at = $expires_at;

        return $this;
    }

    public function getUsedAt(): ?\DateTimeInterface
    {
        return $this->used_at;
    }

    public function setUsedAt(\DateTimeInterface $used_at): self
    {
        $this->used_at = $used_at;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(?int $status): self
    {
        $this->status = $status;

        return $this;
    }

}

<?php

namespace App\CardBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * Class Configuration
 * @package App\CardBundle\DependencyInjection
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('card');

        $rootNode
            ->children()
                ->arrayNode('card_validity_periods')->prototype('scalar')->end()->end()
                ->scalarNode('card_serie_pattern')->defaultValue('[A-Z]{2}')->end()
                ->scalarNode('card_serie_pattern_description')->defaultValue('Requires two capital letters')->end()
            ->end();

        return $treeBuilder;
    }
}

<?php

namespace App\CardBundle\Admin;

use App\CardBundle\Entity\Card;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\CoreBundle\Form\Type\DateTimePickerType;
use Sonata\DoctrineORMAdminBundle\Filter\ChoiceFilter;
use Sonata\DoctrineORMAdminBundle\Filter\DateTimeFilter;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

/**
 * Class CardAdmin
 * @package App\CardBundle\Admin
 */
class CardAdmin extends AbstractAdmin
{
    /**
     * @var array
     */
    protected $datagridValues = [
        '_sort_by' => 'id',
    ];

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        unset($this->listModes['mosaic']);

        $listMapper
            ->addIdentifier('card_serie', null, ["label" => "Serie"])
            ->addIdentifier('card_number', null, ["label" => "Number"])
            ->addIdentifier('issued_at', null, ["format" => "d.m.Y H:i:s"])
            ->addIdentifier('expires_at', null, ["format" => "d.m.Y H:i:s"])
            ->addIdentifier('status', 'choice', ['choices' => array_flip(Card::getStatusList())])
            ->add('_action', null, [
                'actions' => [
                    'show'          => [],
                    'edit'          => [],
                    'delete'        => [],
                    'toggle_status' => [
                        'template' => '@Card/action_btn_toggle_status.html.twig',
                        'data'     => Card::getStatusList(),
                    ],
                ]
            ]);
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->with(($this->isCurrentRoute('create')) ? 'New card' : (($this->isCurrentRoute('generate')) ? 'Generate cards' : "Edit card"), ['class' => 'col-md-9']);

        $formMapper->add('card_serie', null, [
            "disabled" => ($this->isCurrentRoute('create') || $this->isCurrentRoute('generate')) ? false : true,
            "required" => true,
            "attr"     => [
                "pattern" => $this->getContainer()->getParameter('card.config')['card_serie_pattern'],
                "title"   => $this->getContainer()->getParameter('card.config')['card_serie_pattern_description'],
            ],
        ]);

        $formMapper->add('card_number', TextType::class, [
            "disabled" => true,
            "required" => false,
        ]);

        $formMapper->add('balance', null, [
            "label"    => ($this->isCurrentRoute('create') || $this->isCurrentRoute('generate')) ? "Initial balance" : "Balance",
            "disabled" => ($this->isCurrentRoute('create') || $this->isCurrentRoute('generate')) ? false : true,
        ]);

        if ($this->isCurrentRoute('generate')) {
            $formMapper->add('amount', IntegerType::class, [
                'label'  => 'Amount of cards to generate',
                'data'   => 10,
                'mapped' => false,
            ]);
        }

        $formMapper->end();

        $formMapper->with(' ', ['class' => 'col-md-3']);

        $formMapper->add('status', ChoiceType::class, ['choices' => Card::getStatusList()]);

        if ($this->isCurrentRoute('create') || $this->isCurrentRoute('generate')) {
            $formMapper->add('expires_at', ChoiceType::class, [
                "label"    => "Expires after",
                'mapped'   => false,
                'required' => true,
                'choices'  => $this->getContainer()->getParameter('card.config')['card_validity_periods'],
            ]);
        } else {
            $formMapper->add('expires_at', DateTimePickerType::class, [
                'dp_side_by_side'   => true,
                'dp_use_current'    => true,
                'dp_use_seconds'    => true,
                'dp_collapse'       => true,
                'dp_calendar_weeks' => false,
                'dp_view_mode'      => 'days',
                'dp_min_view_mode'  => 'days',
                'date_format'       => 'dd.MM.yyyy HH:mm:ss',
            ]);
        }

        $formMapper->end();
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('card_serie');
        $datagridMapper->add('card_number');
        $datagridMapper->add('issued_at', DateTimeFilter::class, [
            'field_type' => DateTimePickerType::class,
        ], null, [
            'dp_side_by_side'   => true,
            'dp_use_current'    => true,
            'dp_use_seconds'    => true,
            'dp_collapse'       => true,
            'dp_calendar_weeks' => false,
            'dp_view_mode'      => 'days',
            'dp_min_view_mode'  => 'days',
            'date_format'       => 'dd.MM.yyyy HH:mm:ss',
        ]);
        $datagridMapper->add('expires_at', DateTimeFilter::class, [
            'field_type' => DateTimePickerType::class,
        ], null, [
            'dp_side_by_side'   => true,
            'dp_use_current'    => true,
            'dp_use_seconds'    => true,
            'dp_collapse'       => true,
            'dp_calendar_weeks' => false,
            'dp_view_mode'      => 'days',
            'dp_min_view_mode'  => 'days',
            'date_format'       => 'dd.MM.yyyy HH:mm:ss',
        ]);
        $datagridMapper->add('status', ChoiceFilter::class, [], ChoiceType::class, [
            'choices'  => Card::getStatusList(),
            'multiple' => true,
        ]);
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->tab('Card')
            ->with('General', [
                'class'       => 'col-md-8',
                'box_class'   => 'box box-solid box-primary',
                'description' => 'Card',
            ])
            ->add('card_serie')
            ->add('card_number')
            ->add('balance')
            ->add('status', 'choice', ['choices' => array_flip(Card::getStatusList())])
            ->end()
            ->with('Estimates', [
                'class'       => 'col-md-4',
                'box_class'   => 'box box-solid box-primary',
                'description' => 'Card',
            ])
            ->add('issued_at', null, ["format" => "d.m.Y H:i:s"])
            ->add('expires_at', null, ["format" => "d.m.Y H:i:s"])
            ->add('used_at', null, ["format" => "d.m.Y H:i:s"])
            ->end()
            ->end()
            ->tab('Purchases')
            ->with('Purchases', [
                'class'       => 'col-md-12',
                'box_class'   => 'box box-solid box-primary',
                'description' => 'Purchases',
            ])
            ->add('purchases', null, [
                'label'    => 'Demo mockup data',
                'mapped'   => false,
                'template' => '@Card/purchase_list.html.twig',
                'data'     => [
                    ['id' => 1, 'date' => '01.12.2018 12:00:00', 'amount' => '25.00'],
                    ['id' => 2, 'date' => '01.12.2018 15:00:00', 'amount' => '30.00'],
                    ['id' => 3, 'date' => '01.12.2018 17:15:25', 'amount' => '50.00'],
                ],
            ])
            ->end()
            ->end();
    }

    /**
     * @param $object
     * @throws \Exception
     */
    public function prePersist($object)
    {
        $repo = $this->getContainer()->get("doctrine.orm.entity_manager")->getRepository(Card::class);
        $object->setCardNumber($repo->generateNumber($object->getCardSerie()));
        $issued_at = new \DateTime();
        $object->setIssuedAt($issued_at);
        $expires_at = new \DateTime();
        $expires_at->modify('+' . $this->getForm()->get("expires_at")->getData());
        $object->setExpiresAt($expires_at);
        parent::prePersist($object);
    }

    /**
     * @return null|\Symfony\Component\DependencyInjection\ContainerInterface
     */
    public function getContainer()
    {
        return $this->getConfigurationPool()->getContainer();
    }

    /**
     * @param RouteCollection $collection
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('toggle_status', $this->getRouterIdParameter() . '/toggle_status');
        $collection->add('generate');
    }

    /**
     * @param $action
     * @param null $object
     * @return array
     */
    public function configureActionButtons($action, $object = null)
    {
        $list = parent::configureActionButtons($action, $object);

        if (!in_array($action, ['generate'])
            && $this->hasAccess('edit')
            && $this->hasRoute('generate')
        ) {
            $list['generate']['template'] = '@Card/generate_button.html.twig';
        }

        if (\in_array($action, ['create', 'show', 'edit', 'delete', 'acl', 'batch'])
            && $this->hasAccess('list')
            && $this->hasRoute('list')
        ) {
            $list['list'] = [
                // NEXT_MAJOR: Remove this line and use commented line below it instead
                'template' => $this->getTemplate('button_list'),
                // 'template' => $this->getTemplateRegistry()->getTemplate('button_list'),
            ];
        }

        return $list;
    }

    /**
     * @return array
     */
    public function getDashboardActions()
    {
        $actions = parent::getDashboardActions();

        $actions['generate']['template'] = '@Card/dashboard_button.html.twig';

        return $actions;
    }

}

<?php

namespace App\CardBundle\Controller;

use App\CardBundle\Entity\Card;
use Sonata\AdminBundle\Controller\CRUDController as Controller;

use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormRenderer;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Class CardAdminController
 * @package App\CardBundle\Controller
 */
class CardAdminController extends Controller
{
    /**
     * @param $id
     * @return RedirectResponse
     */
    public function toggleStatusAction($id)
    {
        $object = $this->admin->getSubject();

        if (!$object) {
            throw new NotFoundHttpException(sprintf('unable to find the object with id: %s', $id));
        }

        if ($object->getStatus() == Card::STATUS_ACTIVE) {
            $object->setStatus(Card::STATUS_INACTIVE);
            $this->admin->update($object);
        } elseif ($object->getStatus() == Card::STATUS_INACTIVE) {
            $object->setStatus(Card::STATUS_ACTIVE);
            $this->admin->update($object);
        } else {
            $this->addFlash('sonata_flash_error', 'Status cannot be changed');
        }

        $this->addFlash('sonata_flash_success', 'Status changed successfully');

        return new RedirectResponse($this->admin->getRequest()->headers->get('referer'));

    }

    /**
     * Create action.
     *
     * @throws AccessDeniedException If access is not granted
     * @throws \RuntimeException     If no editable field is defined
     * @throws \ReflectionException
     * @throws \Twig_Error_Runtime
     * @throws \Exception
     *
     * @return Response
     */
    public function generateAction()
    {
        $request = $this->getRequest();

        $this->admin->checkAccess('create');

        $class = new \ReflectionClass($this->admin->hasActiveSubClass() ? $this->admin->getActiveSubClass() : $this->admin->getClass());

        if ($class->isAbstract()) {
            return $this->renderWithExtraParams(
                '@SonataAdmin/CRUD/select_subclass.html.twig',
                [
                    'base_template' => $this->getBaseTemplate(),
                    'admin'         => $this->admin,
                    'action'        => 'generate',
                ],
                null
            );
        }

        $newObject = $this->admin->getNewInstance();
        $this->admin->setSubject($newObject);

        $form = $this->admin->getForm();
        \assert($form instanceof Form);

        if (!\is_array($fields = $form->all()) || 0 === \count($fields)) {
            throw new \RuntimeException(
                'No editable field defined. Did you forget to implement the "configureFormFields" method?'
            );
        }

        $form->setData($newObject);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $isFormValid = $form->isValid();

            if ($isFormValid && (!$this->isInPreviewMode() || $this->isPreviewApproved())) {
                $submittedObject = $form->getData();

                $this->admin->setSubject($submittedObject);
                $this->admin->checkAccess('create', $submittedObject);

                $metadata = $this->getDoctrine()->getManager()->getClassMetadata(get_class($submittedObject));
                $uow = $this->getDoctrine()->getManager()->getUnitOfWork();
                $persister = $uow->getEntityPersister($metadata->getName());

                $this->admin->prePersist($submittedObject);

                try {

                    for ($i = 1; $i <= $form->get("amount")->getData(); $i++) {
                        $clonedObject = clone $submittedObject;
                        $clonedObject->setId(null);
                        $clonedObject->setCardNumber($clonedObject->getCardNumber() + $i - 1);
                        $persister->addInsert($clonedObject);
                        $uow->computeChangeSet($metadata, $clonedObject);
                    }
                    $persister->executeInserts();

                    if ($this->isXmlHttpRequest()) {
                        return $this->renderJson([
                            'result' => 'ok',
                        ], 200, []);
                    }

                    $this->addFlash('sonata_flash_success', $this->trans('Card generation successful', [], 'SonataAdminBundle'));

                    return $this->redirectToList();
                } catch (\Exception $e) {
                    $this->handleModelManagerException($e);

                    $isFormValid = false;
                }
            }

            // show an error message if the form failed validation
            if (!$isFormValid) {
                if (!$this->isXmlHttpRequest()) {
                    $this->addFlash(
                        'sonata_flash_error',
                        $this->trans(
                            'flash_create_error',
                            ['%name%' => $this->escapeHtml($this->admin->toString($newObject))],
                            'SonataAdminBundle'
                        )
                    );
                }
            }
        }

        $formView = $form->createView();

        // set the theme for the current Admin Form
        $twig = $this->get('twig');
        $twig->getRuntime(FormRenderer::class)->setTheme($formView, $this->admin->getFormTheme());

        return $this->renderWithExtraParams("@Card/generate.html.twig", [
            'action'   => 'generate',
            'form'     => $formView,
            'object'   => $newObject,
            'objectId' => null,
        ], null);
    }

}